# custom_component to get info about a random beer
![Version](https://img.shields.io/badge/version-0.0.2-green.svg?style=for-the-badge)

A platform which allows you to get information about a random BrewDog beer.

To get started:   
Put `/custom_components/sensor/brewdog.py` here:  
`<config directory>/custom_components/sensor/brewdog.py`

Example configuration.yaml:  
```yaml
sensor:
  - platform: brewdog
```
[Demo running in Home-Assistant](https://ha-test-brewdog.halfdecent.io/)
### Sample overview:
![Sample overview](overview.png)